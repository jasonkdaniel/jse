package com.exception;
// user defined exception 

public class PositiveNumberException extends Exception {

	private String whatMessage;

	public PositiveNumberException(String whatMessage) {
		super();
		this.whatMessage = whatMessage;
	}

	@Override
	public String getMessage() {
		return this.whatMessage;
	}

	// override

}
