package com.main;

import com.service.Calculator;

public class CalculatorMain {

	public static void main(String[] args) {
		Calculator calculator = new Calculator();
		try {
			int var1 = Integer.parseInt(args[0]); // convert string to integer == Wrapper class
			int var2 = Integer.parseInt(args[1]);

			int ans = calculator.div(var1, var2); // command line interface
			System.out.println("answer : " + ans);
		} catch (java.lang.ArithmeticException | java.lang.ArrayIndexOutOfBoundsException e ) {  
			// solution for the exception
			System.err.println("Solution" + e.getMessage()); // out == correct err
		}

		
		
		catch (java.lang.NumberFormatException var) {
			System.err.println("Please enter the argument" + var.getMessage());
		}
		
		catch (java.lang.Exception var) {
			System.err.println("Please global " + var.getMessage());
		}
		System.out.println("The End");
	}

}
