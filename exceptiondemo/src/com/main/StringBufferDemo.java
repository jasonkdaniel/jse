package com.main;

public class StringBufferDemo {

	public static void main(String[] args) {
		String var1 = "Hello";
		System.out.println("Str - length " + var1.length());
		
		StringBuffer var2 = new StringBuffer(); // web application 
		var2.append("Hello");
		System.out.println("SB - length " + var2.length());
		System.out.println("SB capacity" + var2.capacity());
		// buffer space == we can use 
		var2.append("world");
		System.out.println("After append : " + var2);
		System.out.println("after append SB - length " + var2.length());
		System.out.println("after append SB capacity" + var2.capacity());
		
		var2.append("1234567");		//17 * 2  == // (old capacity * twice)  
		System.out.println("After append123 : " + var2);
		System.out.println("after append SB - length " + var2.length());
		System.out.println("after append SB capacity" + var2.capacity());
		System.out.println(var2.reverse());
		
		StringBuilder var3 = new StringBuilder("Hello");
		
		System.out.println("SBuilder " + var3 );
		System.out.println("after append SBuilder - length " + var3.length());
		System.out.println("after append SBbuilder capacity" + var3.capacity());
	}

}
