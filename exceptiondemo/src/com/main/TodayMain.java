package com.main;

import java.io.FileNotFoundException;

import com.exception.PositiveNumberException;
import com.service.Calculator;

public class TodayMain {

	public static void main(String[] args) {
		
		Calculator calculator = null;
		try {
			calculator= new Calculator();

			int result = calculator.div(10, 0);
			if (result == 0) {
				System.out.println("Somthing is wrong");
			} else {
				System.out.println("Division result : " + result);
			}
		} catch (PositiveNumberException e) {
			System.err.println(e.getMessage());
		} finally {
			System.out.println("Finally block"); // Why -- Release the memory object
			//success of failure 
			calculator = null;
		}

		System.out.println("End of application");

	}

}
